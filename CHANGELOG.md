# Release Notes for 2.x

## [2.0.0 (2021-01-11)](https://bitbucket.org/teamhelium/com.helium.php.api-wrapper/branches/compare/2.0.0%0D1.2.0#diff)
### Changed
- Refactored to be backwards-compatible with PHP 7
- Renamed `Resource` to `ApiResource` to avoid conflict with PHP `Resource` class
- Replaced [`danielstjules/stringy`](https://packagist.org/packages/danielstjules/stringy) with [`symfony/string`](https://packagist.org/packages/symfony/string)
- Changed default operation implementations to assume plural resource names
- Replaced [`doctrine/collections`](https://packagist.org/packages/doctrine/collections) with [`illuminate/collections`](https://packagist.org/packages/illuminate/collections)
- Changed `Resource::all` operation return type from `array` to `Collection`
### Added
- `ApiResource::castMany` method for casting array of `ApiResource` data arrays to array of `ApiResource` instances

## [2.0.1 (2021-02-26)](https://bitbucket.org/teamhelium/com.helium.php.api-wrapper/branches/compare/2.0.1%0D2.0.0#diff)
### Changed
- Renamed project to `com.helium.php.api-wrapper`
- Renamed `CHANGELOG-2.x.md` to `CHANGELOG.md`
### Removed
- Removed `CHANGELOG-1.x.md`
