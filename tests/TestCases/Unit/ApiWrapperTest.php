<?php

namespace Tests\TestCases\Unit;

use Helium\ApiWrapper\Api\Endpoint;
use Helium\ApiWrapper\Api\Route;
use Helium\ApiWrapper\ApiWrapper;
use PHPUnit\Framework\TestCase;

class ApiWrapperTest extends TestCase
{
    /**
     * @runInSeparateProcess
     */
    public function testLoad()
    {
        ApiWrapper::load(__DIR__ . '/../../Assets/routes/test.php');

        $route = Route::find('file.test');
        $this->assertInstanceOf(Endpoint::class, $route);
    }
}