<?php

namespace Tests\Assets\Resource\Resources;

use Helium\ApiWrapper\Resource\Contracts\All as AllContract;
use Helium\ApiWrapper\Resource\Contracts\Create as CreateContract;
use Helium\ApiWrapper\Resource\Contracts\Delete as DeleteContract;
use Helium\ApiWrapper\Resource\Contracts\Get as GetContract;
use Helium\ApiWrapper\Resource\Contracts\Update as UpdateContract;
use Helium\ApiWrapper\Resource\Operations\All;
use Helium\ApiWrapper\Resource\Operations\Create;
use Helium\ApiWrapper\Resource\Operations\Delete;
use Helium\ApiWrapper\Resource\Operations\Get;
use Helium\ApiWrapper\Resource\Operations\Update;
use Helium\ApiWrapper\Resource\ApiResource;

class BasicApiResource extends ApiResource implements AllContract, CreateContract, DeleteContract, GetContract, UpdateContract
{
    use All;
    use Create;
    use Delete;
    use Get;
    use Update;

    public $idField = 'id';

    public $attributes = [
        'id' => 1
    ];

    public $dirty = [];

    public $casts = [];

    public $didGetAttributes = [];

    public $didSetAttributes = [];

    public function getAttribute(string $key)
    {
        $this->didGetAttributes[] = $key;
        return parent::getAttribute($key);
    }

    public function setAttribute(string $key, $value): ApiResource
    {
        $this->didSetAttributes[] = $key;
        return parent::setAttribute($key, $value);
    }
}