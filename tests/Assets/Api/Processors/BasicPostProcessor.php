<?php

namespace Tests\Assets\Api\Processors;

use Helium\ApiWrapper\Api\Processor;
use Helium\ApiWrapper\Api\Request;
use Helium\ApiWrapper\Api\Response;

class BasicPostProcessor extends Processor
{
    public static $called = false;

    public static function handle(Request $request, callable $next): Response
    {
        $response = $next($request);

        static::$called = true;

        return $response;
    }
}