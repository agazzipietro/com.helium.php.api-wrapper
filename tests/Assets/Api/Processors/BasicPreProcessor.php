<?php

namespace Tests\Assets\Api\Processors;

use Helium\ApiWrapper\Api\Processor;
use Helium\ApiWrapper\Api\Request;
use Helium\ApiWrapper\Api\Response;

class BasicPreProcessor extends Processor
{
    public static $called = false;

    public static function handle(Request $request, callable $next): Response
    {
        static::$called = true;

        return $next($request);
    }
}